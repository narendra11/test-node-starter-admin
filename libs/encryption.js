const bcrypt = require('bcrypt');
const SALT_ROUNDS = 10;

function hashPassword(password) {
  if (password != null) {
    return bcrypt.hashSync(password, SALT_ROUNDS);
  }
  return '';
}

module.exports = {
  hashPassword,
};
